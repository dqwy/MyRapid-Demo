/*******************************************************************************
 * Copyright © 2010-2020  陈恩点版权所有
 * Author: 陈恩点
 * Last Updated: 2017/8/21 11:49:53
 * Contact: 18115503914
 * Description: MyRapid快速开发框架
*********************************************************************************/
namespace MyRapid.Client
{
    partial class MainUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainUI));
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gcLeft = new DevExpress.XtraEditors.GroupControl();
            this.accTree = new DevExpress.XtraBars.Navigation.AccordionControl();
            this.smallIconList = new DevExpress.Utils.ImageCollection(this.components);
            this.myBar = new DevExpress.XtraBars.BarManager(this.components);
            this.MyQuickMenu = new DevExpress.XtraBars.Bar();
            this.barMenu = new DevExpress.XtraBars.BarButtonItem();
            this.barSkin = new DevExpress.XtraBars.BarButtonItem();
            this.mySkin = new DevExpress.XtraBars.Ribbon.GalleryDropDown(this.components);
            this.barHelp = new DevExpress.XtraBars.BarButtonItem();
            this.MyStatus = new DevExpress.XtraBars.Bar();
            this.barUserNick = new DevExpress.XtraBars.BarButtonItem();
            this.barPageName = new DevExpress.XtraBars.BarButtonItem();
            this.barAuther = new DevExpress.XtraBars.BarButtonItem();
            this.barVersion = new DevExpress.XtraBars.BarButtonItem();
            this.barTip = new DevExpress.XtraBars.BarButtonItem();
            this.barLook = new DevExpress.XtraBars.BarAndDockingController(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barCloseThis = new DevExpress.XtraBars.BarButtonItem();
            this.barCloseOther = new DevExpress.XtraBars.BarButtonItem();
            this.barCloseLeft = new DevExpress.XtraBars.BarButtonItem();
            this.barCloseRight = new DevExpress.XtraBars.BarButtonItem();
            this.barCloseAll = new DevExpress.XtraBars.BarButtonItem();
            this.repositoryItemPopupGalleryEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupGalleryEdit();
            this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
            this.largeIconList = new DevExpress.Utils.ImageCollection(this.components);
            this.myMdi = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(this.components);
            this.closeMenu = new DevExpress.XtraBars.PopupMenu(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcLeft)).BeginInit();
            this.gcLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.accTree)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.smallIconList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.myBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mySkin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barLook)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupGalleryEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.largeIconList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.myMdi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.closeMenu)).BeginInit();
            this.SuspendLayout();
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // gcLeft
            // 
            this.gcLeft.Controls.Add(this.accTree);
            this.gcLeft.CustomHeaderButtons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraEditors.ButtonsPanelControl.GroupBoxButton("Button", ((System.Drawing.Image)(resources.GetObject("gcLeft.CustomHeaderButtons"))), -1, DevExpress.XtraEditors.ButtonPanel.ImageLocation.Default, DevExpress.XtraBars.Docking2010.ButtonStyle.CheckButton, "", false, -1, true, null, true, false, true, null, null, -1)});
            this.gcLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.gcLeft.Location = new System.Drawing.Point(0, 31);
            this.gcLeft.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gcLeft.Name = "gcLeft";
            this.gcLeft.Size = new System.Drawing.Size(221, 516);
            this.gcLeft.TabIndex = 4;
            this.gcLeft.CustomButtonClick += new DevExpress.XtraBars.Docking2010.BaseButtonEventHandler(this.gcLeft_CustomButtonClick);
            this.gcLeft.CustomButtonChecked += new DevExpress.XtraBars.Docking2010.BaseButtonEventHandler(this.gcLeft_CustomButtonChecked);
            // 
            // accTree
            // 
            this.accTree.DistanceBetweenRootGroups = 0;
            this.accTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.accTree.ExpandElementMode = DevExpress.XtraBars.Navigation.ExpandElementMode.Single;
            this.accTree.Location = new System.Drawing.Point(2, 45);
            this.accTree.Name = "accTree";
            this.accTree.ScrollBarMode = DevExpress.XtraBars.Navigation.ScrollBarMode.Auto;
            this.accTree.ShowFilterControl = DevExpress.XtraBars.Navigation.ShowFilterControl.Auto;
            this.accTree.Size = new System.Drawing.Size(217, 469);
            this.accTree.TabIndex = 0;
            this.accTree.Text = "accordionControl1";
            this.accTree.ElementClick += new DevExpress.XtraBars.Navigation.ElementClickEventHandler(this.accTree_ElementClick);
            this.accTree.ExpandStateChanged += new DevExpress.XtraBars.Navigation.ExpandStateChangedEventHandler(this.accTree_ExpandStateChanged);
            // 
            // smallIconList
            // 
            this.smallIconList.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("smallIconList.ImageStream")));
            // 
            // myBar
            // 
            this.myBar.AllowQuickCustomization = false;
            this.myBar.AllowShowToolbarsPopup = false;
            this.myBar.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.MyQuickMenu,
            this.MyStatus});
            this.myBar.Controller = this.barLook;
            this.myBar.DockControls.Add(this.barDockControlTop);
            this.myBar.DockControls.Add(this.barDockControlBottom);
            this.myBar.DockControls.Add(this.barDockControlLeft);
            this.myBar.DockControls.Add(this.barDockControlRight);
            this.myBar.Form = this;
            this.myBar.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barMenu,
            this.barHelp,
            this.barUserNick,
            this.barPageName,
            this.barAuther,
            this.barVersion,
            this.barTip,
            this.barSkin,
            this.barCloseThis,
            this.barCloseOther,
            this.barCloseLeft,
            this.barCloseRight,
            this.barCloseAll});
            this.myBar.MaxItemId = 20;
            this.myBar.StatusBar = this.MyStatus;
            // 
            // MyQuickMenu
            // 
            this.MyQuickMenu.BarName = "Tools";
            this.MyQuickMenu.DockCol = 0;
            this.MyQuickMenu.DockRow = 0;
            this.MyQuickMenu.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.MyQuickMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Caption, this.barMenu, "菜单"),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSkin),
            new DevExpress.XtraBars.LinkPersistInfo(this.barHelp)});
            this.MyQuickMenu.OptionsBar.AllowQuickCustomization = false;
            this.MyQuickMenu.OptionsBar.DisableCustomization = true;
            this.MyQuickMenu.OptionsBar.DrawDragBorder = false;
            this.MyQuickMenu.OptionsBar.UseWholeRow = true;
            this.MyQuickMenu.Text = "Tools";
            // 
            // barMenu
            // 
            this.barMenu.Caption = "菜单";
            this.barMenu.Id = 0;
            this.barMenu.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barMenu.ImageOptions.Image")));
            this.barMenu.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barMenu.ImageOptions.LargeImage")));
            this.barMenu.Name = "barMenu";
            this.barMenu.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barMenu_ItemClick);
            // 
            // barSkin
            // 
            this.barSkin.ActAsDropDown = true;
            this.barSkin.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barSkin.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.barSkin.Caption = "皮肤";
            this.barSkin.DropDownControl = this.mySkin;
            this.barSkin.Id = 11;
            this.barSkin.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barSkin.ImageOptions.Image")));
            this.barSkin.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barSkin.ImageOptions.LargeImage")));
            this.barSkin.Name = "barSkin";
            // 
            // mySkin
            // 
            this.mySkin.Manager = this.myBar;
            this.mySkin.Name = "mySkin";
            // 
            // barHelp
            // 
            this.barHelp.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barHelp.Caption = "帮助文档";
            this.barHelp.Id = 3;
            this.barHelp.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barHelp.ImageOptions.Image")));
            this.barHelp.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barHelp.ImageOptions.LargeImage")));
            this.barHelp.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F1);
            this.barHelp.Name = "barHelp";
            this.barHelp.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barHelp_ItemClick);
            // 
            // MyStatus
            // 
            this.MyStatus.BarName = "Status bar";
            this.MyStatus.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.MyStatus.DockCol = 0;
            this.MyStatus.DockRow = 0;
            this.MyStatus.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.MyStatus.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barUserNick, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barPageName, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barAuther, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barVersion, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barTip, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.MyStatus.OptionsBar.AllowQuickCustomization = false;
            this.MyStatus.OptionsBar.DisableCustomization = true;
            this.MyStatus.OptionsBar.DrawDragBorder = false;
            this.MyStatus.OptionsBar.UseWholeRow = true;
            this.MyStatus.Text = "Status bar";
            // 
            // barUserNick
            // 
            this.barUserNick.Caption = "Admin";
            this.barUserNick.Id = 4;
            this.barUserNick.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barUserNick.ImageOptions.Image")));
            this.barUserNick.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barUserNick.ImageOptions.LargeImage")));
            this.barUserNick.Name = "barUserNick";
            // 
            // barPageName
            // 
            this.barPageName.Caption = "CBest";
            this.barPageName.Id = 5;
            this.barPageName.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barPageName.ImageOptions.Image")));
            this.barPageName.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barPageName.ImageOptions.LargeImage")));
            this.barPageName.Name = "barPageName";
            // 
            // barAuther
            // 
            this.barAuther.Caption = "Auther";
            this.barAuther.Id = 6;
            this.barAuther.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barAuther.ImageOptions.Image")));
            this.barAuther.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barAuther.ImageOptions.LargeImage")));
            this.barAuther.Name = "barAuther";
            // 
            // barVersion
            // 
            this.barVersion.Caption = "Version";
            this.barVersion.Id = 7;
            this.barVersion.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barVersion.ImageOptions.Image")));
            this.barVersion.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barVersion.ImageOptions.LargeImage")));
            this.barVersion.Name = "barVersion";
            // 
            // barTip
            // 
            this.barTip.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barTip.Caption = "12:00";
            this.barTip.Id = 9;
            this.barTip.Name = "barTip";
            // 
            // barLook
            // 
            this.barLook.PaintStyleName = "Skin";
            this.barLook.PropertiesBar.AllowLinkLighting = false;
            this.barLook.PropertiesBar.DefaultGlyphSize = new System.Drawing.Size(16, 16);
            this.barLook.PropertiesBar.DefaultLargeGlyphSize = new System.Drawing.Size(32, 32);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.myBar;
            this.barDockControlTop.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlTop.Size = new System.Drawing.Size(774, 31);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 547);
            this.barDockControlBottom.Manager = this.myBar;
            this.barDockControlBottom.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlBottom.Size = new System.Drawing.Size(774, 27);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 31);
            this.barDockControlLeft.Manager = this.myBar;
            this.barDockControlLeft.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 516);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(774, 31);
            this.barDockControlRight.Manager = this.myBar;
            this.barDockControlRight.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 516);
            // 
            // barCloseThis
            // 
            this.barCloseThis.Caption = "关闭当前";
            this.barCloseThis.Id = 12;
            this.barCloseThis.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barCloseThis.ImageOptions.Image")));
            this.barCloseThis.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barCloseThis.ImageOptions.LargeImage")));
            this.barCloseThis.Name = "barCloseThis";
            this.barCloseThis.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barCloseThis_ItemClick);
            // 
            // barCloseOther
            // 
            this.barCloseOther.Caption = "关闭其他";
            this.barCloseOther.Id = 13;
            this.barCloseOther.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barCloseOther.ImageOptions.Image")));
            this.barCloseOther.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barCloseOther.ImageOptions.LargeImage")));
            this.barCloseOther.Name = "barCloseOther";
            this.barCloseOther.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barCloseOther_ItemClick);
            // 
            // barCloseLeft
            // 
            this.barCloseLeft.Caption = "关闭左侧";
            this.barCloseLeft.Id = 14;
            this.barCloseLeft.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barCloseLeft.ImageOptions.Image")));
            this.barCloseLeft.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barCloseLeft.ImageOptions.LargeImage")));
            this.barCloseLeft.Name = "barCloseLeft";
            this.barCloseLeft.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barCloseLeft_ItemClick);
            // 
            // barCloseRight
            // 
            this.barCloseRight.Caption = "关闭右侧";
            this.barCloseRight.Id = 15;
            this.barCloseRight.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barCloseRight.ImageOptions.Image")));
            this.barCloseRight.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barCloseRight.ImageOptions.LargeImage")));
            this.barCloseRight.Name = "barCloseRight";
            this.barCloseRight.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barCloseRight_ItemClick);
            // 
            // barCloseAll
            // 
            this.barCloseAll.Caption = "关闭全部";
            this.barCloseAll.Id = 16;
            this.barCloseAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barCloseAll.ImageOptions.Image")));
            this.barCloseAll.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barCloseAll.ImageOptions.LargeImage")));
            this.barCloseAll.Name = "barCloseAll";
            this.barCloseAll.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barCloseAll_ItemClick);
            // 
            // repositoryItemPopupGalleryEdit1
            // 
            this.repositoryItemPopupGalleryEdit1.AutoHeight = false;
            this.repositoryItemPopupGalleryEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupGalleryEdit1.Name = "repositoryItemPopupGalleryEdit1";
            // 
            // splitterControl1
            // 
            this.splitterControl1.Location = new System.Drawing.Point(221, 31);
            this.splitterControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.splitterControl1.Name = "splitterControl1";
            this.splitterControl1.Size = new System.Drawing.Size(5, 516);
            this.splitterControl1.TabIndex = 11;
            this.splitterControl1.TabStop = false;
            // 
            // largeIconList
            // 
            this.largeIconList.ImageSize = new System.Drawing.Size(32, 32);
            this.largeIconList.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("largeIconList.ImageStream")));
            // 
            // myMdi
            // 
            this.myMdi.ClosePageButtonShowMode = DevExpress.XtraTab.ClosePageButtonShowMode.InAllTabPagesAndTabControlHeader;
            this.myMdi.FloatOnDoubleClick = DevExpress.Utils.DefaultBoolean.True;
            this.myMdi.FloatOnDrag = DevExpress.Utils.DefaultBoolean.True;
            this.myMdi.Images = this.smallIconList;
            this.myMdi.MdiParent = this;
            this.myMdi.SelectedPageChanged += new System.EventHandler(this.MyMdi_SelectedPageChanged);
            this.myMdi.MouseUp += new System.Windows.Forms.MouseEventHandler(this.MyMdi_MouseUp);
            this.myMdi.PageAdded += new DevExpress.XtraTabbedMdi.MdiTabPageEventHandler(this.MyMdi_PageAdded);
            this.myMdi.PageRemoved += new DevExpress.XtraTabbedMdi.MdiTabPageEventHandler(this.MyMdi_PageRemoved);
            // 
            // closeMenu
            // 
            this.closeMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barCloseThis),
            new DevExpress.XtraBars.LinkPersistInfo(this.barCloseOther),
            new DevExpress.XtraBars.LinkPersistInfo(this.barCloseLeft),
            new DevExpress.XtraBars.LinkPersistInfo(this.barCloseRight),
            new DevExpress.XtraBars.LinkPersistInfo(this.barCloseAll)});
            this.closeMenu.Manager = this.myBar;
            this.closeMenu.Name = "closeMenu";
            // 
            // MainUI
            // 
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(774, 574);
            this.Controls.Add(this.splitterControl1);
            this.Controls.Add(this.gcLeft);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Location = new System.Drawing.Point(0, 0);
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.Name = "MainUI";
            this.Activated += new System.EventHandler(this.MainUIEx_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainUI_FormClosing);
            this.Load += new System.EventHandler(this.MainUI_Load);
            this.Shown += new System.EventHandler(this.MainUI_Shown);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.gcLeft, 0);
            this.Controls.SetChildIndex(this.splitterControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcLeft)).EndInit();
            this.gcLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.accTree)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.smallIconList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.myBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mySkin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barLook)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupGalleryEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.largeIconList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.myMdi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.closeMenu)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraEditors.GroupControl gcLeft;
        private DevExpress.XtraEditors.SplitterControl splitterControl1;
        private DevExpress.XtraBars.BarManager myBar;
        private DevExpress.XtraBars.Bar MyQuickMenu;
        private DevExpress.XtraBars.Bar MyStatus;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem barMenu;
        private DevExpress.XtraBars.BarButtonItem barHelp;
        private DevExpress.XtraBars.BarButtonItem barUserNick;
        private DevExpress.XtraBars.BarButtonItem barPageName;
        private DevExpress.XtraBars.BarButtonItem barAuther;
        private DevExpress.XtraBars.BarButtonItem barVersion;
        private DevExpress.XtraBars.BarButtonItem barTip;
        private DevExpress.Utils.ImageCollection smallIconList;
        private DevExpress.Utils.ImageCollection largeIconList;
        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager myMdi;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupGalleryEdit repositoryItemPopupGalleryEdit1;
        private DevExpress.XtraBars.BarButtonItem barSkin;
        private DevExpress.XtraBars.Ribbon.GalleryDropDown mySkin;
        private DevExpress.XtraBars.PopupMenu closeMenu;
        private DevExpress.XtraBars.BarButtonItem barCloseThis;
        private DevExpress.XtraBars.BarButtonItem barCloseOther;
        private DevExpress.XtraBars.BarButtonItem barCloseLeft;
        private DevExpress.XtraBars.BarButtonItem barCloseRight;
        private DevExpress.XtraBars.BarButtonItem barCloseAll;
        private DevExpress.XtraBars.Navigation.AccordionControl accTree;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraBars.BarAndDockingController barLook;
    }
}